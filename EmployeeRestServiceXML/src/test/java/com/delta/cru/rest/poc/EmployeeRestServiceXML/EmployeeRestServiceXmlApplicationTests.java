package com.delta.cru.rest.poc.EmployeeRestServiceXML;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
@SpringBootConfiguration
public class EmployeeRestServiceXmlApplicationTests {
	@Test
	public void contextLoads() {
	}

}
