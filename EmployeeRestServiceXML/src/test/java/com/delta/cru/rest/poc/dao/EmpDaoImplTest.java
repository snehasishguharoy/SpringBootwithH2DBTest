/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;

import com.delta.cru.rest.poc.exp.DaoException;
import com.delta.cru.rest.poc.mapper.EmployeeMapper;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EmpDaoImplTest {
	@InjectMocks
	private EmpDaoImpl daoImpTest;
	@Mock
	private EmployeeMapper employeeMapper;
	
	@Rule 
	public ExpectedException exct=ExpectedException.none();

	@Test
	@Ignore
	public void getAllEmpDetailsTest() throws DaoException {

		Mockito.when(daoImpTest.getAllEmpDetails()).thenThrow(DataAccessException.class);
	//	daoImpTest.getAllEmpDetails();

	}
	@Ignore
	@Test(expected=DaoException.class)
	public void getAllEmpDetailsTest1() throws DaoException {
		Mockito.doThrow(new DaoException("hello")).when(employeeMapper).getAllEmpDetails();
	}

}
