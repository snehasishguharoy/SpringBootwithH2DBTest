package com.delta.cru.rest.poc.config;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

import com.delta.cru.rest.poc.vo.EmpVo;
import com.delta.cru.rest.poc.vo.EmplPhVo;

@Configuration
@MappedTypes(value = { EmpVo.class, EmplPhVo.class })
@MapperScan("com.delta.cru.rest.poc.mapper")
public class EmployeeConfig {
//	@Profile("dvl")
//	@Bean
//	@ConfigurationProperties(prefix = "datasource")
//	public DataSource dataSource() {
//		return DataSourceBuilder.create().build();
//	}
//
//	@Profile("dvl")
//	@Bean
//	public SqlSessionFactory sessionFactory() throws Exception {
//		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//		bean.setDataSource(dataSource());
//		return bean.getObject();
//	}

}
