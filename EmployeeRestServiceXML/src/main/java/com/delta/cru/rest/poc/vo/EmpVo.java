package com.delta.cru.rest.poc.vo;

import java.io.Serializable;

public class EmpVo implements Serializable {
	private static final long serialVersionUID = 8642735862039852046L;

	private int emplId;

	private String fname;

	private String lname;

	private String address;

	public EmpVo(Integer emplId, String fname, String lname, String address, String city) {
		super();
		this.emplId = emplId;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.city = city;
	}

	private String city;

	public int getEmplId() {
		return emplId;
	}

	public void setEmplId(int emplId) {
		this.emplId = emplId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public EmpVo() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
