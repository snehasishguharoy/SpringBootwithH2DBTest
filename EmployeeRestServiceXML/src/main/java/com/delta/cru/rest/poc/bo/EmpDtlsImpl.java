package com.delta.cru.rest.poc.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.delta.cru.rest.poc.dao.EmpDaoIfc;
import com.delta.cru.rest.poc.exp.DaoException;
import com.delta.cru.rest.poc.vo.EmpVo;

@Component
public class EmpDtlsImpl implements EmpDtlsIfc {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmpDtlsImpl.class);

	@Autowired
	private EmpDaoIfc employeeDaoImpl;

	@Override
	public EmpVo fndEmpDetailById(int id) throws DaoException {
		LOGGER.info("In the business layer");
		return employeeDaoImpl.fndEmployeeDetailById(id);
	}

}
