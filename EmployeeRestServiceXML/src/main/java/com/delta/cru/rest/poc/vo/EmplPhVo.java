package com.delta.cru.rest.poc.vo;

import java.io.Serializable;
import java.util.Date;

public class EmplPhVo implements Serializable {
	private static final long serialVersionUID = -8643993196757108186L;

	private String emplId;

	private String phTypCd;

	private String emplPhNb;

	private Date effDt;

	private Date expDt;

	public String getEmplId() {
		return emplId;
	}

	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}

	public String getPhTypCd() {
		return phTypCd;
	}

	public void setPhTypCd(String phTypCd) {
		this.phTypCd = phTypCd;
	}

	public EmplPhVo() {
		super();
	}

	public String getEmplPhNb() {
		return emplPhNb;
	}

	public void setEmplPhNb(String emplPhNb) {
		this.emplPhNb = emplPhNb;
	}

	public Date getEffDt() {
		return effDt;
	}

	public void setEffDt(Date effDt) {
		this.effDt = effDt;
	}

	public Date getExpDt() {
		return expDt;
	}

	public void setExpDt(Date expDt) {
		this.expDt = expDt;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
