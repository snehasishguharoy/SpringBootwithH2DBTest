package com.delta.cru.rest.poc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.delta.cru.rest.poc.mapper.EmployeeMapper;
import com.delta.cru.rest.poc.vo.EmpVo;

@Repository
public class EmpDaoImpl implements EmpDaoIfc {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpDaoImpl.class);

	@Autowired
	private EmployeeMapper employeeMapper;

	@Override
	public EmpVo fndEmployeeDetailById(int id) {
		LOGGER.info("In the DAO Layer");
		return employeeMapper.fndEmployeeDetailById(id);
	}


}
