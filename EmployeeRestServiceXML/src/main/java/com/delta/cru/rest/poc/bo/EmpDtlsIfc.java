package com.delta.cru.rest.poc.bo;

import com.delta.cru.rest.poc.exp.DaoException;
import com.delta.cru.rest.poc.vo.EmpVo;

public interface EmpDtlsIfc {
	EmpVo fndEmpDetailById(int id) throws DaoException;
	
//	EmpVo findEmpById(int empId)  throws EmployeeNotFoundException;


}
