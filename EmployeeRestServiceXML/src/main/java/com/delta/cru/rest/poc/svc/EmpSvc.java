package com.delta.cru.rest.poc.svc;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.cru.rest.poc.bo.EmpDtlsIfc;
import com.delta.cru.rest.poc.exp.DaoException;
import com.delta.cru.rest.poc.vo.EmpVo;

@RestController
public class EmpSvc {

	private static final Logger LOGGER = Logger.getLogger(EmpSvc.class.getName());

	@Autowired
	private EmpDtlsIfc dtlsIfc;

	@RequestMapping(value = "/name/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public @ResponseBody EmpVo getEmpDetailById(@PathVariable("id")int id) throws DaoException  {
		LOGGER.info("Printing the entire employee details");
		return dtlsIfc.fndEmpDetailById(id);
	}

//	@RequestMapping(value = "/names/{empid}", method = RequestMethod.GET, produces = { "application/json" })
//	public @ResponseBody EmpVo findEmpById(@PathVariable("empid") int empid) throws EmployeeNotFoundException {
//		LOGGER.info("Printing the particular employee detail");
//		return dtlsIfc.findEmpById(empid);
//
//	}

}
