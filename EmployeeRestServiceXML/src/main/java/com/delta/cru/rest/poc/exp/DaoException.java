package com.delta.cru.rest.poc.exp;

public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;

	public DaoException(String message) {
		super(message);
	}

}
