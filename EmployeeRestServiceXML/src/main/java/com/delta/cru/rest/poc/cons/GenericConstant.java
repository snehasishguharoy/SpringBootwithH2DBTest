package com.delta.cru.rest.poc.cons;
public class GenericConstant {
	/** The Constant JNDI_NAME. */
	public static final String JNDI_NAME = "jdbc/oracle";

	private GenericConstant() {
		super();
	}
}
